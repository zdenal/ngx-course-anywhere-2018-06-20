import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Car } from './../cars.model';
import { CarsService } from './../cars.service';

@Component({
  selector: 'app-cars-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateComponent {

  i18NPrefix = 'cars.create.';

  constructor(
    private carsService: CarsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  onClose(): void {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  onCreate(car: Car): void {
    this.router.navigate(['..', 'detail', car.id], { relativeTo: this.route });
  }

}
