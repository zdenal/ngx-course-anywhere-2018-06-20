import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { cars } from './../../mocks/cars';
import { CarsApiService } from './cars-api.service';
import { Car, Cars } from './cars.model';

const delay = 1000;

@Injectable()
export class CarsApiMockedService extends CarsApiService {

  getCars$(): Observable<Cars> {
    return Observable.timer(delay).map(() => cars);
  }

  getCar$(id: string): Observable<Car> {
    return Observable.timer(delay).map(() => {
      const car = cars.find((c) => c.id === id);
      if (!car) {
        throw new Error(`Car with id ${id} not exists.`);
      }
      return car;
    });
  }

  updateCar$(car: Car): Observable<Car> {
    return Observable.timer(delay).map(() => {
      const idx = cars.findIndex((c) => c.id === car.id);
      if (idx === -1) {
        throw new Error(`Car with id ${car.id} not exists.`);
      }
      cars[idx] = car;
      return car;
    });
  }

  createCar$(car: Car): Observable<Car> {
    return Observable.timer(delay).map(() => {
      car.id = `${new Date().getTime()}`;
      cars.push(car);
      return car;
    });
  }

  deleteCar$(id: string): Observable<any> {
    return Observable.timer(delay).map(() => {
      const idx = cars.findIndex((c) => c.id === id);
      if (idx === -1) {
        throw new Error(`Car with id ${id} not exists.`);
      }
      cars.splice(idx, 1);
      return null;
    });
  }

}
