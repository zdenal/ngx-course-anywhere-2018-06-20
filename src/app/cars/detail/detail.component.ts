import { ChangeDetectorRef, Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Car } from './../cars.model';
import { CarsService } from './../cars.service';

@Component({
  selector: 'app-cars-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailComponent implements OnDestroy {

  i18NPrefix = 'cars.detail.';

  car: Car;
  loading = false;
  loadingError = false;
  editing = false;
  deleting = false;
  deleteError = false;

  private carSub: Subscription;

  constructor(
    private carsService: CarsService,
    private translate: TranslateService,
    private cd: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.paramMap.do(() => {
      this.editing = false;
      this.car = null;
      this.loading = true;
      this.loadingError = false;
      this.cd.markForCheck();
    }).map((paramMap) => paramMap.get('carId')).mergeMap((id) => {
      return this.carsService.getCar$(id).catch((e) => {
        console.error(e);
        return Observable.of(null);
      });
    }).subscribe((car) => {
      this.car = car;
      this.carsService.selectCar(car);
      this.loading = false;
      this.loadingError = !this.car;
      this.cd.markForCheck();
    });
  }

  ngOnDestroy() {
    this.carSub && this.carSub.unsubscribe();
    this.carsService.deselectCar();
  }

  onClose(): void {
    this.router.navigate(['../..'], { relativeTo: this.route });
  }

  onEdit(): void {
    this.editing = true;
  }

  onEditClose(car: Car): void {
    this.editing = false;
  }

  onEditSave(car: Car): void {
    this.editing = false;
    this.car = car;
    this.cd.markForCheck();
  }

  onDelete(): void {
    this.translate.get(this.i18NPrefix + 'deleteConfirmation', {
      name: `${this.car.brand} ${this.car.model}`
    }).first().subscribe((confirmMsg) => {
      if (confirm(confirmMsg)) {
        this.deleting = true;
        this.deleteError = false;

        this.carsService.deleteCar$(this.car.id).subscribe(() => {
          this.deleting = false;
          this.cd.markForCheck();
          this.onClose();
        }, (e) => {
          console.error(e);
          this.deleteError = true;
          this.deleting = false;
          this.cd.markForCheck();
        });
      }
    });
  }

}
