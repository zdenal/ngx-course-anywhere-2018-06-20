import { ContactComponent } from './contact/contact.component';
import { ContactModule } from './contact/contact.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CarsComponent } from './cars/cars.component';
import { CarsModule } from './cars/cars.module';
import { CreateComponent } from './cars/create/create.component';
import { DetailComponent } from './cars/detail/detail.component';
import { HttpStatus404Component } from './http-status-404/http-status-404.component';

@NgModule({
  imports: [
    CarsModule,
    ContactModule,
    RouterModule.forRoot(
      [
        {
          path: '',
          redirectTo: 'cars',
          pathMatch: 'full'
        },
        {
          path: 'cars',
          component: CarsComponent,
          children: [
            {
              path: 'create',
              component: CreateComponent
            },
            {
              path: 'detail/:carId',
              component: DetailComponent
            }
          ]
        },
        {
          path: 'contact',
          component: ContactComponent
        },
        {
          path: '**',
          component: HttpStatus404Component
        }
      ],
      {
        enableTracing: false
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
