import { Cars } from './../app/cars/cars.model';

export const cars: Cars = [
  {
    id: '-L6ubMOHdZJolhd0IQaT',
    brand: 'Škoda',
    model: 'Superb',
    fuel: 'diesel',
    power: 120,
    description: 'Perfect czech car'
  },
  {
    id: '-L6ucUBi9wWPjs_htuXJ',
    brand: 'Volkswagen',
    model: 'Golf',
    fuel: 'petrol',
    power: 100,
    description: 'Basic small car with long history'
  },
  {
    id: '-L6uctIP9LRh3qA1xncn',
    brand: 'Porsche',
    model: '911',
    fuel: 'petrol',
    power: 270
  }
];
